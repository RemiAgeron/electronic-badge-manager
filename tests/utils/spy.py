from env.door import Door

class DoorSpy(Door):
    def __init__(self) -> None:
        super().__init__()
        self.opened = False
        self.count = 0

    def send_open(self):
        self.opened = True
        self.count += 1
