from env.engine import Engine
from env.reader import Badge
from .fake import FakeClock, FakeReader, FakeDoor
from datetime import datetime

class BaseBuilder:
    def build(self):
        return self


class EngineBuilder(Engine, BaseBuilder):
    def connect_clock(self, clock: FakeClock):
        Engine.connect_clock(self, clock)
        return self

    def link(self, reader: FakeReader, door: FakeDoor):
        Engine.link(self, reader, door)
        return self

    def block(self, badge: Badge):
        Engine.block(self, badge)
        return self

class ReaderBuilder(FakeReader, BaseBuilder):
    def simulate(self, badge: Badge):
        FakeReader.simulate(self, badge)
        return self

class BadgeBuilder(Badge, BaseBuilder):
    pass

class DoorBuilder(FakeDoor, BaseBuilder):
    def block(self):
        FakeDoor.block(self)
        return self

class ClockBuilder(FakeClock, BaseBuilder):
    def set_date(self, date: datetime):
        FakeClock.set_date(self, date)
        return self


class Builder:
    def __init__(self, reader_base_name: str) -> None:
        self.badge_id = 0
        self.reader_id = 0
        self.reader_base = reader_base_name

    def engine(self) -> EngineBuilder:
        return EngineBuilder()

    def badge(self) -> BadgeBuilder:
        self.badge_id += 1
        return BadgeBuilder(self.badge_id)

    def reader(self) -> ReaderBuilder:
        self.reader_id += 1
        return ReaderBuilder(f"{self.reader_base}{self.reader_id}")

    def door(self) -> DoorBuilder:
        return DoorBuilder()

    def clock(self) -> ClockBuilder:
        return ClockBuilder()
