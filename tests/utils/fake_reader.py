from env.reader import Badge, Reader

class FakeReader(Reader):
    def __init__(self, id: str) -> None:
        super().__init__(id)
        self.badge: Badge | None = None

    def request(self):
        result = self.badge
        self.badge = None
        return result

    def simulate(self, badge: Badge):
        self.badge = badge
