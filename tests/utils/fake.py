from env.clock import Clock
from env.door import DoorBlocked
from env.reader import Badge, Reader
from .spy import DoorSpy

class FakeClock(Clock):
    def set_date(self, date):
        self.date = date

    def get_date(self):
        return self.date

class FakeReader(Reader):
    def __init__(self, id: str) -> None:
        super().__init__(id)
        self.badge: Badge | None = None

    def request(self):
        result = self.badge
        self.badge = None
        return result

    def simulate(self, badge: Badge):
        self.badge = badge

class FakeDoor(DoorSpy):
    def send_open(self):
        if self.blocked:
            raise DoorBlocked
        DoorSpy.send_open(self)

    def block(self):
        self.blocked = True

    def unblock(self):
        self.blocked = False
