from .utils.builder import Builder
from datetime import datetime
import pytest

@pytest.fixture
def builder() -> Builder:
    return Builder(reader_base_name="lecteur")

"""
1 ligne / tentative
{datetime} - {badge} - {lecteur} - OK / KO
alphaNum maj lecteur
num badge
tentatives réussies
signal envoyé aux portes du lecteur OK
signal refusé en cas de porte bloqué DOOR BLOCKED
tentative échoué
aucun badge détecté par le lecteur KO

Ajouter mode verbose pour ne pas enregistré les tentatives OK
"""

first_date = datetime(2024, 1, 1, 0, 1)
second_date = datetime(2024, 1, 1, 0, 1, 10)

def test_clock_base(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge, avec une Horloge qui affiche 01/01/2024 à 00:01 connectée au Moteur d'Ouverture
    door = builder.door().build()
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    clock = builder.clock().set_date(first_date).build()
    engine = builder.engine().link(reader, door).connect_clock(clock).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS une nouvelle entrée dans le journal renseigne sur la tentative d'ouverture: la date, le badge, le lecteur et le status
    assert "01/01/2024 00:01:00 - BADGE 1 - LECTEUR1 - OK" in engine.logs

def test_clock_no_badge(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, n'ayant pas détecté de Badge, avec une Horloge qui affiche 01/01/2024 à 00:01 connectée au Moteur d'Ouverture
    door = builder.door().build()
    reader = builder.reader().build()
    clock = builder.clock().set_date(first_date).build()
    engine = builder.engine().link(reader, door).connect_clock(clock).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS une nouvelle entrée dans le journal renseigne sur la tentative d'ouverture: la date, le badge, le lecteur et le status
    assert "01/01/2024 00:01:00 - NO BADGE - LECTEUR1 - KO" in engine.logs

def test_clock_no_link(builder: Builder):
    # ÉTANT DONNÉ une Porte et un Lecteur non lié, ayant détecté un Badge, avec une Horloge qui affiche 01/01/2024 à 00:01 connectée au Moteur d'Ouverture
    door = builder.door().build()
    reader = builder.reader().build()
    clock = builder.clock().set_date(first_date).build()
    engine = builder.engine().connect_clock(clock).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS une nouvelle entrée dans le journal renseigne sur la tentative d'ouverture: la date, le badge, le lecteur et le status
    assert not engine.logs

def test_clock_multi_run(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge, avec une Horloge qui affiche 01/01/2024 à 00:01 connectée au Moteur d'Ouverture
    door = builder.door().build()
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    clock = builder.clock().set_date(first_date).build()
    engine = builder.engine().link(reader, door).connect_clock(clock).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ÉTANT DONNÉ la même Porte liée au même Lecteur ayant détecté un autre Badge et l'Horloge affiche 01/01/2024 à 00:01:10
    other_badge = builder.badge().build()
    reader.simulate(other_badge)
    clock.set_date(second_date)

    # QUAND le Moteur d'Ouverture effectue une nouvelle interrogation de chaque Lecteur
    engine.run()

    # ALORS une nouvelle entrée dans le journal renseigne sur la tentative d'ouverture: la date, le badge, le lecteur et le status
    assert "01/01/2024 00:01:00 - BADGE 1 - LECTEUR1 - OK" in engine.logs
    assert "01/01/2024 00:01:10 - BADGE 2 - LECTEUR1 - OK" in engine.logs

def test_clock_multi_door_KO(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge, avec une Horloge connectée au Moteur d'Ouverture
    # ÉTANT DONNÉ une autre Porte bloquée liée au même Lecteur
    door = builder.door().build()
    door_blocked = builder.door().block().build()
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    clock = builder.clock().set_date(first_date).build()
    engine = builder.engine().link(reader, door).link(reader, door_blocked).connect_clock(clock).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur et que l'Horloge affiche 22/04/2024 à 17h34 et 59.444 secondes
    engine.run()

    # ALORS une nouvelle entrée dans le journal renseigne sur la tentative d'ouverture: la date, le badge, le lecteur et le status
    assert "01/01/2024 00:01:00 - BADGE 1 - LECTEUR1 - DOOR BLOCKED" in engine.logs
