from .utils.builder import Builder
import pytest

@pytest.fixture
def builder() -> Builder:
    return Builder(reader_base_name="lecteur")

def test_base(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la Porte
    assert door.opened

def test_no_badge(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur
    reader = builder.reader().build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS aucun signal d'ouverture n'a été envoyé à la Porte
    assert not door.opened

def test_no_link(builder: Builder):
    # ÉTANT DONNÉ une Porte et un Lecteur non lié, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS aucun signal d'ouverture n'a été envoyé à la Porte
    assert not door.opened

def test_multi_link(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    # ÉTANT DONNÉ une autre Porte liée à un autre Lecteur, n'ayant pas détecté de Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    other_reader = builder.reader().build()
    other_door = builder.door().build()
    engine = builder.engine().link(reader, door).link(other_reader, other_door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la seconde Porte et non à la première
    assert door.opened
    assert not other_door.opened

def test_multi_link_reversed(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    # ÉTANT DONNÉ une autre Porte liée à un autre Lecteur, n'ayant pas détecté de Badge
    badge = builder.badge().build()
    reader = builder.reader().build()
    door = builder.door().build()
    other_reader = builder.reader().simulate(badge).build()
    other_door = builder.door().build()
    engine = builder.engine().link(reader, door).link(other_reader, other_door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la première Porte et pas à la seconde
    assert not door.opened
    assert other_door.opened

def test_multi_door(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    # ÉTANT DONNÉ une autre Porte liée au même Lecteur
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    other_door = builder.door().build()
    engine = builder.engine().link(reader, door).link(reader, other_door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation du Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à chaque Porte
    assert door.opened
    assert other_door.opened

def test_multi_reader_door(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, n'ayant pas détecté de Badge
    # ÉTANT DONNÉ la même Porte liée à un différent Lecteur, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    other_reader = builder.reader().build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).link(other_reader, door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS un unique signal d'ouverture est envoyé à la Porte
    assert door.opened
    assert door.count == 1

def test_multi_reader_door_reversed(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    # ÉTANT DONNÉ la même Porte liée à un différent Lecteur, n'ayant pas détecté de Badge
    badge = builder.badge().build()
    reader = builder.reader().build()
    other_reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).link(other_reader, door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la Porte
    assert door.opened
    assert door.count == 1

def test_no_run(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).build()

    # QUAND le Moteur d'Ouverture n'effectue pas l'interrogation de chaque Lecteur

    # ALORS le signal d'ouverture est n'envoyé pas à la Porte
    assert not door.opened

def test_multi_run(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).build()

    # QUAND le Moteur d'Ouverture effectue plusieurs interrogation de chaque Lecteur
    engine.run()
    engine.run()

    # ALORS un unique signal d'ouverture est envoyé à la Porte
    assert door.opened
    assert door.count == 1

def test_badge_blocked(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge bloqué
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).block(badge).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture n'est pas envoyé à la Porte
    assert not door.opened

def test_badge_blocked_restore(builder: Builder):
    # ÉTANT DONNÉ une Porte liée à un Lecteur, ayant détecté un Badge bloqué qui a été restoré
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().build()
    engine = builder.engine().link(reader, door).block(badge).build()
    engine.restore(badge)

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la Porte
    assert door.opened

def test_door_blocked(builder: Builder):
    # ÉTANT DONNÉ une Porte bloquée liée à un Lecteur, ayant détecté un Badge
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().block().build()
    engine = builder.engine().link(reader, door).build()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la Porte mais est refusé
    assert not door.opened
    assert "NO DATE - BADGE 1 - LECTEUR1 - DOOR BLOCKED" in engine.logs

def test_door_blocked_restore(builder: Builder):
    # ÉTANT DONNÉ une Porte, bloquée puis restorée, liée à un Lecteur, ayant détecté un Badge bloqué
    badge = builder.badge().build()
    reader = builder.reader().simulate(badge).build()
    door = builder.door().block().build()
    engine = builder.engine().link(reader, door).build()
    door.unblock()

    # QUAND le Moteur d'Ouverture effectue une interrogation de chaque Lecteur
    engine.run()

    # ALORS le signal d'ouverture est envoyé à la Porte
    assert door.opened
