class Badge:
    def __init__(self, id: int) -> None:
        self.id = id

class Reader:
    def __init__(self, id: str) -> None:
        self.badge: Badge
        self.id = id.upper()

    def request(self) -> Badge | None:
        pass
