from env.clock import Clock
from env.reader import Reader, Badge
from env.door import Door, DoorBlocked

class Link:
    def __init__(self, reader: Reader, door: Door) -> None:
        self.reader = reader
        self.door_set = set([door])

    def add_door(self, door: Door):
        self.door_set.add(door)

class Engine:
    def __init__(self) -> None:
        self.linked = set()
        self.blocked = set()
        self.logs = []
        self.clock: Clock | None = None

    def link(self, reader: Reader, door: Door):
        if reader not in [link.reader for link in self.linked]:
            self.linked.add(Link(reader, door))
        else:
            for link in self.linked:
                if reader == link.reader:
                    link.add_door(door)

    def run(self):
        for link in self.linked:
            badge = link.reader.request()
            status = "KO"
            if self.access(badge):
                status = "OK"
                for door in link.door_set:
                    try:
                        door.send_open()
                    except DoorBlocked:
                        status = "DOOR BLOCKED"
            self.log(badge, link.reader, status)

    def access(self, badge: Badge):
        return badge and not badge in self.blocked

    def block(self, badge: Badge):
        self.blocked.add(badge)

    def restore(self, badge: Badge):
        self.blocked.remove(badge)

    def connect_clock(self, clock: Clock):
        self.clock = clock

    def log(self, badge: Badge, reader: Reader, status: str = "NO STATUS"):
        date = self.clock.get_date().strftime("%d/%m/%Y %H:%M:%S") if self.clock else "NO DATE"
        badge_id = f"BADGE {badge.id}" if badge else "NO BADGE"
        reader_id = reader.id if reader else "NO READER"
        self.logs.append(f"{date} - {badge_id} - {reader_id} - {status}")
